var animation = require('alloy/animation');

// When clicking on the red button the red window opens
function redwinclick (e) {
	var redwin = Alloy.createController("redwindow").getView();
	redwin.open();
}

// When clicking on the blue button the blue window opens
function bluewinclick (e) {
	var bluewin = Alloy.createController("bluewindow").getView();
	bluewin.open();
}

// When clicking on the yellow button the yellow window opens
// while fliping from the left
function yellowwinclick (e) {
	var yellowwin = Alloy.createController("yellowwindow").getView();
	yellowwin.open({
		transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
	});
}

// When clicking on the image button the image window opens
function seeimage (e) {
	var imageview = Alloy.createController("imagewindow").getView();
	imageview.open();
}

// When clicking on the animation the label shakes,
// then opens the animation window animated transition
function animatewin (e) {
  animation.shake($.animationWindow, 30, function  () {
	  var animationwin = Alloy.createController("animatewindow").getView();
	  animationwin.open({
	  	transition: Ti.UI.iPhone.AnimationStyle.CURL_DOWN
	  }); 	
  });
}

function tablewin () {
	var tablewin = Alloy.createController("tablewindow").getView();
	tablewin.open({
		transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT
	});
}

// function webwin () {
// 	var webwin = Alloy.createController("webview").getView();
// 	webwin.open({
// 		transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT
// 	});
// }

function webwin () {
		var webwin = Alloy.createController("webview").getView();

		webwin.open();
}

function mapwin (e) {
	var mapwin = Alloy.createController("mapview").getView();
	mapwin.open();
}

$.mainWindow.open();

