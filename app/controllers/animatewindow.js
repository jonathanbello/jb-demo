// Close the animation window
function closeAnimateWin () {
	$.animateWindow.close({
		transition: Ti.UI.iPhone.AnimationStyle.CURL_UP
	});
}

function doAnimation () {
	animation.shake($.theBox, 100);
}

function showIndicators () {
	$.activityIndicator2.show();
	$.activityIndicator.show();

	setTimeout(function(){
        $.activityIndicator2.hide();
    }, 3000);
}